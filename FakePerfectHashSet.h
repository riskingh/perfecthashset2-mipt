//
//  FakePerfectHashSet.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_FakePerfectHashSet_h
#define PerfectHashSet2_FakePerfectHashSet_h

#include <map>
#include <vector>

#include "PerfectHashSet.h"

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NTests {
    class CFakePerfectHashSet: public NPerfectHashSet::IHashSet {
    private:
        std::map<ui, bool> data;
        ui size;
        
        bool insertOrRemove(ui _key, bool isInsert) {
            if (has(_key) == isInsert)
                return false;
            data[_key] = isInsert;
            size += isInsert ? 1 : -1;
            return true;
        }
        
    public:
        CFakePerfectHashSet()
        : IHashSet(), size(0) {}
        
        void init(const std::vector<ui> &_keys) {
            for (const auto &key: _keys) {
                if (data.find(key) != data.end())
                    throw NPerfectHashSet::CEqualKeysException(key);
                data.insert(std::pair<ui, bool>(key, false));
            }
        }
        
        bool possibleKey(ui _key) const {
            return data.find(_key) != data.end();
        }
        
        void checkPossibility(ui _key) const {
            if (!possibleKey(_key))
                throw NPerfectHashSet::CImpossibleKeyException(_key);
        }
        
        bool has(ui _key) const {
            checkPossibility(_key);
            return data.find(_key)->second;
        }
        
        bool insert(ui _key) {
            return insertOrRemove(_key, true);
        }
        
        bool erase(ui _key) {
            return insertOrRemove(_key, false);
        }
        
        ui getSize() const {
            return size;
        }
    };
}

#endif
