//
//  RandomNumber.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_RandomNumber_h
#define PerfectHashSet2_RandomNumber_h

#include <random>
#include "Functions.h"

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NPerfectHashSet {
    template<class _T>
    class CRandomNumber {
    private:
        std::default_random_engine generator;
        std::uniform_int_distribution<_T> distribution;
    public:
        CRandomNumber(_T _min, _T _max)
        : distribution(std::uniform_int_distribution<_T>(_min, _max)) {
        }
        _T operator ()() {
            return distribution(generator);
        }
    };
    
    CRandomNumber<ull> randomNumber0(0, ULL_PRIME - 1), randomNumber1(1, ULL_PRIME - 1);
}

#endif
