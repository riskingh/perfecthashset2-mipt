//
//  HashSet.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_HashSet_h
#define PerfectHashSet2_HashSet_h

#include "Functions.h"
#include "CommonException.h"

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NPerfectHashSet {
    class IHashSet {
    public:
        IHashSet () {}
        
        virtual void init(const std::vector<ui> &_keys) = 0;
        virtual bool possibleKey(ui _key) const = 0;
        virtual bool has(ui _key) const = 0;
        virtual bool insert(ui _key) = 0;
        virtual bool erase(ui _key) = 0;
        virtual ui getSize() const = 0;
    };
    
    class CImpossibleKeyException: public CCommonException {
    public:
        CImpossibleKeyException(ui _key)
        : CCommonException("Impossible key", _key) {}
    };
    
    class CEqualKeysException: public CCommonException {
    public:
        CEqualKeysException(ui _key)
        : CCommonException("Equal keys", _key) {}
    };
    
    class CHashSet: public IHashSet {
    private:
        std::vector<std::pair<ull, bool>> data;
        ui size;
        CHashParams hashParams;
        
        ui index(ui _key) const {
            return (ui)(hash(_key, hashParams) % (ull)data.size());
        }
    public:
        CHashSet()
        : IHashSet() {}
        
        void init(const std::vector<ui> &_keys) {
            ui equalKey;
            if (hasEqual(_keys, equalKey)) throw CEqualKeysException(equalKey);
            size = 0;
            bool hasCollisions;
            ui keyIndex;
            do {
                hasCollisions = false;
                data.assign(pow2(_keys.size()), std::pair<ull, bool>((ull)-1, true));
                hashParams = CHashParams(randomNumber1(), randomNumber0());
                for (const auto &key: _keys) {
                    keyIndex = index(key);
                    if (!data[index(key)].second) {
                        hasCollisions = true;
                        break;
                    }
                    data[keyIndex].first = key;
                    data[keyIndex].second = false;
                }
            }
            while (hasCollisions);
        }
        
        bool possibleKey(ui _key) const {
            if (data.size() == 0) return false;
            return data[index(_key)].first == _key;
        }
        
        void checkPossibility(ui _key) const {
            if (!possibleKey(_key))
                throw CImpossibleKeyException(_key);
        }
        
        bool has(ui _key) const {
            checkPossibility(_key);
            return data[index(_key)].second;
        }
        
        bool insertOrRemove(ui _key, bool isInsert) {
            checkPossibility(_key);
            if (has(_key) == isInsert) return false;
            data[index(_key)].second = isInsert;
            size += isInsert ? 1 : -1;
            return true;
        }
        
        bool insert(ui _key) {
            return insertOrRemove(_key, true);
        }
        
        bool erase(ui _key) {
            return insertOrRemove(_key, false);
        }
        
        ui getSize() const {
            return size;
        }
    };
}

#endif
