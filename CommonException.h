//
//  CommonException.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_CommonException_h
#define PerfectHashSet2_CommonException_h

#include <string>
#include <sstream>

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NPerfectHashSet {
    class CCommonException: public std::exception {
    public:
        const std::string message;
        ui key;
        
        CCommonException(std::string _message, ui _key)
        : std::exception(), message(_message), key(_key) {}
        
        virtual const char *what() const throw() {
            std::stringstream ss;
            ss << message << " (key: " << key << ")\n";
            std::string s;
            getline(ss, s);
            return s.c_str();
        }
    };
}

#endif
