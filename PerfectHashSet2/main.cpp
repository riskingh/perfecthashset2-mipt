//
//  main.cpp
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#include <iostream>
#include <string>

#include "PerfectHashSet.h"
#include "FakePerfectHashSet.h"
#include "Tests.h"

typedef unsigned int ui;
typedef unsigned long long ull;

int main(int argc, const char * argv[]) {
    std::string testType;
    std::cout << "unique - toggles unique keys, compares with map,\npermutation,\ntypical - checks for equal keys,\nimpossible - inserts possible and impossible keys,\ntime - measures time of init and insert,\nall.";
    std::cin >> testType;
    ui tests, keys, queries, min, max, type;
    if (testType == "unique") {
        std::cout << "testUnique(ui _tests, ui _keys, ui _queries, ui _min, ui _max)\n";
        std::cin >> tests >> keys >> queries >> min >> max;
        NTests::testUnique(tests, keys, queries, min, max);
    }
    else if (testType == "permutation") {
        std::cout << "testPermutation(ui _keys)\n";
        std::cin >> keys;
        NTests::testPermutation(keys);
    }
    else if (testType == "typical") {
        std::cout << "testTypical(ui _tests, ui _keys, ui _min, ui _max, ui _type)\n";
        std::cout << "TYPES:\n1: a a a a a a ...\n2: a b a b a b ...\n3: a a b b c c ...\n4: pair of equal and others are distinct\n";
        std::cin >> tests >> keys >> min >> max >> type;
        NTests::testTypical(tests, keys, min, max, (NTests::ETestType)type);
    }
    else if (testType == "impossible") {
        std::cout << "testImpossibleKeys(ui _tests, ui _keys, ui _queries, ui _min, ui _max)\n";
        std::cin >> tests >> keys >> queries >> min >> max;
        NTests::testImpossibleKeys(tests, keys, queries, min, max);
    }
    else if (testType == "time") {
        std::cout << "testTime(ui _tests, ui _keys, ui _queries, ui _min, ui _max)\n";
        std::cin >> tests >> keys >> queries >> min >> max;
        NTests::testTime(tests, keys, queries, min, max);
    }
    else if (testType == "all") {
        std::cout << "testAll(ui _tests, ui _keys, ui _queries, ui _min, ui _max)\ntestPermutation will be called only if _keys < 14\n";
        std::cin >> tests >> keys >> queries >> min >> max;
        NTests::testAll(tests, keys, queries, min, max);
    }
    else {}
    return 0;
}
