//
//  PerfectHashSet.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_PerfectHashSet_h
#define PerfectHashSet2_PerfectHashSet_h

#include "RandomNumber.h"
#include "HashSet.h"
#include "CommonException.h"

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NPerfectHashSet {
    class CPerfectHashSet: public IHashSet {
    private:
        std::vector<CHashSet> data;
        CHashParams hashParams;
        ui size;
        
        ui index(ui _key) const {
            return (ui)(hash(_key, hashParams) % (ull)data.size());
        }
    public:
        CPerfectHashSet()
        : IHashSet(), size(0) {}
        
        void init(const std::vector<ui> &_keys) {
            size = 0;
            std::vector<std::vector<ui>> firstLevel;
            CHashParams firstLevelParams;
            ull keysSize = (ull)_keys.size(), expectedSize;
            ui keyForException;
            do {
                firstLevel.assign(keysSize, std::vector<ui>());
                firstLevelParams = CHashParams(randomNumber1(), randomNumber0());
                for (const auto &key: _keys)
                    firstLevel[(ui)(hash(key, firstLevelParams) % keysSize)].push_back(key);
                expectedSize = 0ll;
                for (auto &secondLevel: firstLevel) {
                    if (hasEqualNeighbours(secondLevel, keyForException)) throw CEqualKeysException(keyForException);
                    expectedSize += pow2(secondLevel.size());
                }
            }
            while (expectedSize > 3ll * keysSize);
            hashParams = firstLevelParams;
            
            for (int secondLevelIndex = 0; secondLevelIndex < keysSize; ++secondLevelIndex) {
                data.push_back(CHashSet());
                data.back().init(firstLevel[secondLevelIndex]);
            }
        }
        
        bool possibleKey(ui _key) const {
            if (data.size() == 0)
                return false;
            return data[index(_key)].possibleKey(_key);
        }
        
        void checkPossibility(ui _key) const {
            if (!possibleKey(_key))
                throw CImpossibleKeyException(_key);
        }
        
        bool has(ui _key) const {
            checkPossibility(_key);
            return data[index(_key)].has(_key);
        }
        
        bool insertOrRemove(ui _key, bool isInsert) {
            checkPossibility(_key);
            if (!data[index(_key)].insertOrRemove(_key, isInsert))
                return false;
            size += isInsert ? 1 : -1;
            return true;
        }
        
        bool insert(ui _key) {
            return insertOrRemove(_key, true);
        }
        
        bool erase(ui _key) {
            return insertOrRemove(_key, false);
        }
        
        ui getSize() const {
            return size;
        }
    };
}

#endif
