//
//  Functions.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_Functions_h
#define PerfectHashSet2_Functions_h

#include <vector>

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NPerfectHashSet {
    const ull ULL_PRIME = 4294967311ll;
    const ull MOD_64_ULL_PRIME = 225ll;
    
    struct CUllBits32 {
        ull first32bits, second32bits;
        
        explicit CUllBits32(ull _num = 0ll) {
            first32bits = _num >> 32ll;
            second32bits = static_cast<ull>(static_cast<ui>(_num));
        }
        
        CUllBits32(ull _first32bits, ull _second32bits)
        : first32bits(_first32bits), second32bits(_second32bits) {}
    };
    
    struct CHashParams {
        ull firstCoefficent, secondCoefficent;
        
        CHashParams(ull _firstCoefficent = 0ll, ull _secondCoefficent = 0ll)
        : firstCoefficent(_firstCoefficent), secondCoefficent(_secondCoefficent) {}
    };
    
    ull mulULL(ull _mul1, ull _mul2) {
        CUllBits32 mul1Bits, mul2Bits;
        mul1Bits = CUllBits32(_mul1);
        mul2Bits = CUllBits32(_mul2);
        
        ull result = 0ll;
        result += (((mul1Bits.first32bits * mul2Bits.first32bits) % ULL_PRIME) * MOD_64_ULL_PRIME) % ULL_PRIME;
        result += ((( ((mul1Bits.first32bits * mul2Bits.second32bits) % ULL_PRIME) + ((mul1Bits.second32bits * mul2Bits.first32bits) % ULL_PRIME) ) % ULL_PRIME) * (1ll << 32ll)) % ULL_PRIME;
        result += (mul1Bits.second32bits * mul2Bits.second32bits) % ULL_PRIME;
        return result;
    }
    
    ull hash(ull _key, CHashParams _params) {
        return (mulULL(_params.firstCoefficent, _key) + _params.secondCoefficent) % ULL_PRIME;
    }
    
    ull pow2(ull _number) {
        return _number * _number;
    }
    
    bool hasEqualNeighbours(const std::vector<ui> &_vector, ui &_key) {
        if (_vector.size() <= 1) return false;
//        _key = _vector[_vector.size() - 1];
        _key = _vector.back();
        for (const auto &curKey: _vector) {
            if (curKey == _key) return true;
            _key = curKey;
        }
        return false;
    }
    
    bool hasEqual(const std::vector<ui> &_vector, ui &_key) {
        for (int i = 0; i < _vector.size(); ++i) {
            for (int j = i + 1; j < _vector.size(); ++j) {
                if (_vector[i] == _vector[j]) {
                    _key = _vector[i];
                    return true;
                }
            }
        }
        return false;
    }
}

#endif
