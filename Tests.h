//
//  Tests.h
//  PerfectHashSet2
//
//  Created by Максим Гришкин on 30/11/14.
//  Copyright (c) 2014 Максим Гришкин. All rights reserved.
//

#ifndef PerfectHashSet2_Tests_h
#define PerfectHashSet2_Tests_h

#include <vector>
#include <set>
#include <algorithm>
#include <ctime>

#include "RandomNumber.h"
#include "PerfectHashSet.h"
#include "FakePerfectHashSet.h"

typedef unsigned int ui;
typedef unsigned long long ull;

namespace NTests {
    std::vector<ui> randomUI(ui _size, ui _min, ui _max) {
        NPerfectHashSet::CRandomNumber<ui> randomNumber(_min, _max);
        std::vector<ui> result(_size);
        std::generate(result.begin(), result.end(), randomNumber);
        return result;
    }
    
    std::vector<ui> uniqueUI(ui _size, ui _min, ui _max) {
        NPerfectHashSet::CRandomNumber<ui> randomNumber(_min, _max);
        std::set<ui> temp;
        while (temp.size() < _size)
            temp.insert(randomNumber());
        std::vector<ui> result(temp.begin(), temp.end());
        std::shuffle(result.begin(), result.end(), std::default_random_engine((ui)time(0)));
        return result;
    }
    
    enum ETestType {
        TT_ALL_EQUAL,
        TT_TWO_KEYS_ALTERNATION,
        TT_DOUBLED_KEYS,
        TT_ALL_UNIQUE_EXCEPT_LAST,
        TT_NUMBER
    };
    
    std::vector<ui> typicalUI(ui _size, ui _min, ui _max, ETestType _type) {
        NPerfectHashSet::CRandomNumber<ui> randomNumber(_min, _max);
        std::vector<ui> result(_size), temp;
        switch (_type) {
            case TT_ALL_EQUAL:
                result.assign(_size, randomNumber());
                break;
            case TT_TWO_KEYS_ALTERNATION:
                temp = uniqueUI(2, _min, _max);
                for (ui index = 0; index < _size; ++index)
                    result[index] = temp[index % 2];
                break;
            case TT_DOUBLED_KEYS:
                temp = uniqueUI((_size + 1) / 2, _min, _max);
                for (ui index = 0; index < _size; ++index)
                    result[index] = temp[index / 2];
                break;
            case TT_ALL_UNIQUE_EXCEPT_LAST:
                result = uniqueUI(_size, _min, _max);
                temp = uniqueUI(2, 0, _size - 1);
                result[temp[0]] = result[temp[1]];
                break;
        }
        return result;
    }
    
    enum EAction {
        HAS,
        SIZE,
        INSERT,
        ERASE,
        TOGGLE
    };
    const EAction fullCheck[] = {HAS, SIZE, TOGGLE, HAS, SIZE};
    const ui fullCheckSize = 5;
    
    bool checkAction(EAction _action, NPerfectHashSet::IHashSet *_hashSet1, NPerfectHashSet::IHashSet *_hashSet2, ui _key = 0) {
        switch (_action) {
            case TOGGLE:
                if (_hashSet1->has(_key)) _action = ERASE;
                else _action = INSERT;
            case HAS:
                return _hashSet1->has(_key) == _hashSet2->has(_key);
            case SIZE:
                return _hashSet1->getSize() == _hashSet2->getSize();
            case INSERT:
                return _hashSet1->insert(_key) == _hashSet2->insert(_key);
            case ERASE:
                return _hashSet1->erase(_key) == _hashSet2->erase(_key);
        }
    }
    
    bool equalResultsFullCheck(ui _key, NPerfectHashSet::IHashSet *_hashSet1, NPerfectHashSet::IHashSet *_hashSet2) {
        for (ui index = 0; index < fullCheckSize; ++index)
            if (!checkAction(fullCheck[index], _hashSet1, _hashSet2, _key)) return false;
        return true;
    }
    
    bool equalResults(std::vector<ui> _keys, NPerfectHashSet::IHashSet *_hashSet1, NPerfectHashSet::IHashSet *_hashSet2) {
        for (const auto &key: _keys)
            if (!equalResultsFullCheck(key, _hashSet1, _hashSet2)) return false;
        return true;
    }
    
    std::vector<ui> randomQueries(ui _queries, const std::vector<ui> &_keys) {
        std::vector<ui> indexes = randomUI(_queries, 0, (ui)_keys.size() - 1);
        std::vector<ui> queries(_queries);
        for (ui i = 0; i < _queries; ++i)
            queries[i] = _keys[indexes[i]];
        return queries;
    }
    
    bool testUnique(ui _tests, ui _keys, ui _queries, ui _min, ui _max) {
        ui correct = 0;
        for (ui test = 0; test < _tests; ++test) {
            NPerfectHashSet::CPerfectHashSet *phs = new NPerfectHashSet::CPerfectHashSet();
            CFakePerfectHashSet *fphs = new CFakePerfectHashSet();
            std::vector<ui> keys = uniqueUI(_keys, _min, _max);
            std::vector<ui> queries = randomQueries(_queries, keys);
            phs->init(keys);
            fphs->init(keys);
            correct += equalResults(queries, phs, fphs);
        }
        std::cout << "Correct/All = " << correct << "/" << _tests << "\n";
        return correct == _tests;
    }
    
    bool testPermutation(ui _keys) {
        ui correct = 0, tests = 0;;
        std::vector<ui> keys(_keys), queries;
        for (ui index = 0; index < _keys; ++index)
            keys[index] = index;
        do {
            NPerfectHashSet::CPerfectHashSet *phs = new NPerfectHashSet::CPerfectHashSet();
            CFakePerfectHashSet *fphs = new CFakePerfectHashSet();
            phs->init(keys);
            fphs->init(keys);
            queries = randomQueries(_keys, keys);
            correct += equalResults(queries, phs, fphs);
            ++tests;
        }
        while (next_permutation(keys.begin(), keys.end()));
        std::cout << "Correct/All = " << correct << "/" << tests << "\n";
        return correct == tests;
    }
    
    bool testTypical(ui _tests, ui _keys, ui _min, ui _max, ETestType _type) {
        ui correct = 0;
        for (ui test = 0; test < _tests; ++test) {
            NPerfectHashSet::CPerfectHashSet phs;
            std::vector<ui> keys = typicalUI(_keys, _min, _max, _type);
            try {
                phs.init(keys);
            }
            catch (const NPerfectHashSet::CEqualKeysException &e) {
                ui equalKeys = 0;
                for (const auto &key: keys)
                    equalKeys += (key == e.key);
                if (equalKeys > 1)
                    ++correct;
            }
        }
        std::cout << "Correct/All = " << correct << "/" << _tests << "\n";
        return correct == _tests;
    }
    
    bool testImpossibleKeys(ui _tests, ui _keys, ui _queries, ui _min, ui _max) {
        ui correct = 0;
        for (ui test = 0; test < _tests; ++test) {
            NPerfectHashSet::CPerfectHashSet phs;
            std::vector<ui> allKeys = uniqueUI(_keys * 2, _min, _max);
            std::vector<ui> keys(allKeys.begin(), allKeys.begin() + _keys);
            phs.init(keys);
            std::set<ui> impossibles(keys.begin() + _keys, keys.end());
            std::vector<ui> queries = randomQueries(_queries, allKeys);
            bool exceptionHappend = false;
            for (const auto &key: queries) {
                try {
                    phs.insert(key);
                }
                catch (const NPerfectHashSet::CImpossibleKeyException &e) {
                    exceptionHappend = true;
                }
                correct += (impossibles.find(key) != impossibles.end()) && exceptionHappend;
            }
        }
        std::cout << "Correct/All = " << correct << "/" << _tests << "\n";
        return correct == _tests;
    }
    
    void testTime(ui _tests, ui _keys, ui _queries, ui _min, ui _max) {
        clock_t sumInit = 0, sumQueries = 0, t1, t2;
        for (ui test = 0; test < _tests; ++test) {
            NPerfectHashSet::CPerfectHashSet phs;
            std::vector<ui> keys = uniqueUI(_keys, _min, _max);
            std::vector<ui> queries = randomQueries(_queries, keys);
            t1 = clock();
            phs.init(keys);
            t2 = clock();
            sumInit += (t2 - t1);
            t1 = clock();
            for (const auto &key: queries)
                phs.insert(key);
            t2 = clock();
            sumQueries += (t2 - t1);
        }
        std::cout << "Total time:         " << (double)(sumInit + sumQueries) / (double)CLOCKS_PER_SEC << "\n";
        std::cout << "Average init time:  " << ((double)sumInit / (double)CLOCKS_PER_SEC) / (double)_tests << "\n";
        std::cout << "Average query time: " << ((double)sumQueries / (double)CLOCKS_PER_SEC) / (double)_tests / (double)_queries << "\n";
    }
    
    void testAll(ui _tests, ui _keys, ui _queries, ui _min, ui _max) {
        testUnique(_tests, _keys, _queries, _min, _max);
        if (_keys <= 13) testPermutation(_keys);
        for (int type = 0; type < TT_NUMBER; ++type)
            testTypical(_tests, _keys, _min, _max, (ETestType)type);
        testImpossibleKeys(_tests, _keys, _queries, _min, _max);
        testTime(_tests, _keys, _queries, _min, _max);
    }
}

#endif
